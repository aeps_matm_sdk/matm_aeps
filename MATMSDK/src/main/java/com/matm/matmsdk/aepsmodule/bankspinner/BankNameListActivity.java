package com.matm.matmsdk.aepsmodule.bankspinner;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import isumatm.androidsdk.equitas.R;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.matm.matmsdk.aepsmodule.utils.AepsSdkConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class BankNameListActivity extends AppCompatActivity implements BankNameContract.View{
    private List<BankNameModel> bankNameModelList = new ArrayList<>();
    private RecyclerView bankNameRecyclerView;
    private BankNameListAdapter bankNameListAdapter;
    private BankNameListPresenter bankNameListPresenter;
    ProgressDialog loadingView;
    SearchView searchView;
    ImageView backImg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_home_spinner_data );

       // setToolbar ();

        bankNameRecyclerView = (RecyclerView) findViewById ( R.id.bankNameRecyclerView );
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager( getApplicationContext () );
        bankNameRecyclerView.setLayoutManager ( mLayoutManager );
        bankNameRecyclerView.setItemAnimator ( new DefaultItemAnimator() );
      //  bankNameRecyclerView.addItemDecoration(new DividerItemDecoration(this));

        bankNameListPresenter = new BankNameListPresenter(BankNameListActivity.this);
        bankNameListPresenter.loadBankNamesList(BankNameListActivity.this);

        backImg = findViewById(R.id.backImg);
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        searchView=(SearchView) findViewById(R.id.searchView);
        searchView.setIconifiedByDefault(true);
        searchView.setQueryHint(getResources().getString(R.string.search_hint));
        searchView.setFocusable(true);
        searchView.setIconified(false);
        searchView.clearFocus();
        searchView.requestFocusFromTouch();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
               // Toast.makeText(getBaseContext(), query, Toast.LENGTH_LONG).show();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                bankNameListAdapter.getFilter().filter(newText);

               //Toast.makeText(getBaseContext(), newText, Toast.LENGTH_LONG).show();
                return true;
            }
        });
       // prepareMovieData ();
    }



    @Override
    public void bankNameListReady(ArrayList<BankNameModel> bankNameModelArrayList) {
        if (bankNameModelArrayList!=null && bankNameModelArrayList.size() > 0){
            bankNameModelList = bankNameModelArrayList;
        }
    }

    @Override
    public void showBankNames() {
        if (bankNameModelList!=null && bankNameModelList.size() > 0){
            bankNameListAdapter = new BankNameListAdapter(bankNameModelList, new BankNameListAdapter.RecyclerViewClickListener() {
                @Override
                public void recyclerViewListClicked(View v, int position) {
                    Intent intent = new Intent();
                    intent.putExtra(AepsSdkConstants.IIN_KEY, bankNameListAdapter.getItem(position));
                    setResult(RESULT_OK, intent);
                    finish();
                }
            });
            bankNameRecyclerView.setAdapter ( bankNameListAdapter );
        }
    }

    @Override
    public void showLoader() {
        if (loadingView ==null){
            loadingView = new ProgressDialog(BankNameListActivity.this);
            loadingView.setCancelable(false);
            loadingView.setMessage("Please wait...");

        }
        loadingView.show();
    }

    @Override
    public void hideLoader() {
        if (loadingView!=null){
            loadingView.dismiss();
        }
    }

    @Override
    public void emptyBanks() {

    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode== KeyEvent.KEYCODE_BACK)
            return false;

        return false;
    }
}